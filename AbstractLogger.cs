﻿/*
 
 Disclaimer: IMPORTANT:  This sample software is supplied to you by Genesys
 Telecommunications Laboratories Inc ("Genesys") in consideration of your agreement
 to the following terms, and your use, installation, modification or redistribution
 of this Genesys software constitutes acceptance of these terms.  If you do not
 agree with these terms, please do not use, install, modify or redistribute this
 Genesys software.
 
 In consideration of your agreement to abide by the following terms, and subject
 to these terms, Genesys grants you a personal, non-exclusive license, under
 Genesys's copyrights in this original Genesys software (the "Genesys Software"), to
 use, reproduce, modify and redistribute the Genesys Software, with or without
 modifications, in source and/or binary forms; provided that if you redistribute
 the Genesys Software in its entirety and without modifications, you must retain
 this notice and the following text and disclaimers in all such redistributions
 of the Genesys Software.
 
 Neither the name, trademarks, service marks or logos of Genesys Inc. may be used
 to endorse or promote products derived from the Genesys Software without specific
 prior written permission from Genesys.  Except as expressly stated in this notice,
 no other rights or licenses, express or implied, are granted by Genesys herein,
 including but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Genesys Software may be
 incorporated.
 
 The Genesys Software is provided by Genesys on an "AS IS" basis.  GENESYS MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDING THE GENESYS SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
 COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL GENESYS BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
 DISTRIBUTION OF THE GENESYS SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
 CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
 GENESYS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Genesys Inc. All Rights Reserved.
 
*/

using System;
using Genesyslab.Platform.Commons.Logging;

namespace Sample.Platform.Logging
{
	/// <summary>
	/// Makes it easier to implement Platform SDK ILogger.
	/// </summary>
	public abstract class AbstractLogger : ILogger
	{
		protected enum LogLevel {
			FatalError,
			Error,
			Warn,
			Info,
			Debug,
		}

		public abstract ILogger CreateChildLogger(string name);

		protected abstract bool IsLevelEnabled(LogLevel logLevel);

		/// <summary>
		/// Default implementation redirects to LogFormat.
		/// </summary>
		protected virtual void LogMessage(LogLevel level, object message)
		{
			LogFormat(level, message.ToString(), new object[0]);
		}

		/// <summary>
		/// Default implementation redirects to LogMessage.
		/// </summary>
		protected virtual void LogException(LogLevel level, object message, Exception exception)
		{
			LogMessage(level, message + " " + exception);
		}

		/// <summary>
		/// Default implementation redirects to LogFormat.
		/// </summary>
		protected virtual void LogFormat(LogLevel level, string format, params object[] args)
		{
			LogMessage(level, string.Format(format, args));
		}

		public bool IsFatalErrorEnabled { get { return IsLevelEnabled(LogLevel.FatalError); } }
		public bool IsErrorEnabled { get { return IsLevelEnabled(LogLevel.Error); } }
		public bool IsWarnEnabled { get { return IsLevelEnabled(LogLevel.Warn); } }
		public bool IsInfoEnabled { get { return IsLevelEnabled(LogLevel.Info); } }
		public bool IsDebugEnabled { get { return IsLevelEnabled(LogLevel.Debug); } }

		public void FatalError(object message) { LogMessage(LogLevel.FatalError, message); }
		public void Error(object message) { LogMessage(LogLevel.Error, message); }
		public void Warn(object message) { LogMessage(LogLevel.Warn, message); }
		public void Info(object message) { LogMessage(LogLevel.Info, message); }
		public void Debug(object message) { LogMessage(LogLevel.Debug, message); }

		public void FatalErrorFormat(string format, params object[] args) { LogFormat(LogLevel.FatalError, format, args); }
		public void ErrorFormat(string format, params object[] args) { LogFormat(LogLevel.Error, format, args); }
		public void WarnFormat(string format, params object[] args) { LogFormat(LogLevel.Warn, format, args); }
		public void InfoFormat(string format, params object[] args) { LogFormat(LogLevel.Info, format, args); }
		public void DebugFormat(string format, params object[] args) { LogFormat(LogLevel.Debug, format, args); }

		public void FatalError(object message, Exception exception) { LogException(LogLevel.FatalError, message, exception); }
		public void Error(object message, Exception exception) { LogException(LogLevel.Error, message, exception); }
		public void Warn(object message, Exception exception) { LogException(LogLevel.Warn, message, exception); }
		public void Info(object message, Exception exception) { LogException(LogLevel.Info, message, exception); }
		public void Debug(object message, Exception exception) { LogException(LogLevel.Debug, message, exception); }
	}
}
