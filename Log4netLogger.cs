﻿/*
 
 Disclaimer: IMPORTANT:  This sample software is supplied to you by Genesys
 Telecommunications Laboratories Inc ("Genesys") in consideration of your agreement
 to the following terms, and your use, installation, modification or redistribution
 of this Genesys software constitutes acceptance of these terms.  If you do not
 agree with these terms, please do not use, install, modify or redistribute this
 Genesys software.
 
 In consideration of your agreement to abide by the following terms, and subject
 to these terms, Genesys grants you a personal, non-exclusive license, under
 Genesys's copyrights in this original Genesys software (the "Genesys Software"), to
 use, reproduce, modify and redistribute the Genesys Software, with or without
 modifications, in source and/or binary forms; provided that if you redistribute
 the Genesys Software in its entirety and without modifications, you must retain
 this notice and the following text and disclaimers in all such redistributions
 of the Genesys Software.
 
 Neither the name, trademarks, service marks or logos of Genesys Inc. may be used
 to endorse or promote products derived from the Genesys Software without specific
 prior written permission from Genesys.  Except as expressly stated in this notice,
 no other rights or licenses, express or implied, are granted by Genesys herein,
 including but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Genesys Software may be
 incorporated.
 
 The Genesys Software is provided by Genesys on an "AS IS" basis.  GENESYS MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDING THE GENESYS SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
 COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL GENESYS BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
 DISTRIBUTION OF THE GENESYS SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
 CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
 GENESYS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Genesys Inc. All Rights Reserved.
 
*/

using System;
using Genesyslab.Platform.Commons.Logging;
using log4net;

namespace Sample.Platform.Logging
{
	public class Log4netLogger : ILogger
	{
		readonly ILog log;

		public Log4netLogger(ILog log)
		{
			this.log = log;
		}

		public ILogger CreateChildLogger(string name)
		{
			var childLog = LogManager.GetLogger(log.Logger.Repository.Name, log.Logger.Name + "." + name);
			return new Log4netLogger(childLog);
		}

		public void FatalError(object message) { log.Fatal(message); }
		public void Error(object message) { log.Error(message); }
		public void Warn(object message) { log.Warn(message); }
		public void Info(object message) { log.Info(message); }
		public void Debug(object message) { log.Debug(message); }

		public void FatalError(object message, Exception exception) { log.Fatal(message, exception); }
		public void Error(object message, Exception exception) { log.Error(message, exception); }
		public void Warn(object message, Exception exception) { log.Warn(message, exception); }
		public void Info(object message, Exception exception) { log.Info(message, exception); }
		public void Debug(object message, Exception exception) { log.Debug(message, exception); }

		public void FatalErrorFormat(string format, params object[] args) { log.FatalFormat(format, args); }
		public void ErrorFormat(string format, params object[] args) { log.ErrorFormat(format, args); }
		public void WarnFormat(string format, params object[] args) { log.WarnFormat(format, args); }
		public void InfoFormat(string format, params object[] args) { log.InfoFormat(format, args); }
		public void DebugFormat(string format, params object[] args) { log.DebugFormat(format, args); }

		public bool IsFatalErrorEnabled { get { return log.IsFatalEnabled; } }
		public bool IsErrorEnabled { get { return log.IsErrorEnabled; } }
		public bool IsWarnEnabled { get { return log.IsWarnEnabled; } }
		public bool IsInfoEnabled { get { return log.IsInfoEnabled; } }
		public bool IsDebugEnabled { get { return log.IsDebugEnabled; } }
	}
}
