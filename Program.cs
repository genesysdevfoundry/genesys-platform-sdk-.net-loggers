﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using log4net;

namespace Sample.Platform.Logging
{
	class Program
	{
		static void Main(string[] args)
		{
			var log = LogManager.GetLogger("MyLogger");


			var changedSource = new TraceSource("Changed");
			changedSource.Switch.Level = SourceLevels.ActivityTracing;

			var changedSource2 = new TraceSource("Changed");

			var nonConfiguredSource = new TraceSource("NonConfigured");
			var configuredSource = new TraceSource("Configured");
			
			var traceSource = new TraceSource("Parent");
			var parentLogger = new TraceSourceLogger(traceSource);
			var childLogger = parentLogger.CreateChildLogger("Child");
			parentLogger.Error("parent error");
			childLogger.Error("child error");
			parentLogger.Error("parent with exception", new Exception("parent exception message"));
			childLogger.Error("child with exception", new Exception("child exception message"));
			traceSource.Flush();
			Console.ReadKey();
		}
	}
}
