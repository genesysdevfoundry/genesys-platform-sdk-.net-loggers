﻿using Genesyslab.Platform.Commons.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Sample.Platform.Logging
{
    public class DebugLogger : AbstractLogger
    {
		readonly string loggerName;
		
		public DebugLogger()
		{
			this.loggerName = "";
		}
			
		public DebugLogger(string loggerName)
		{
			this.loggerName = loggerName;
		}
			
		public override ILogger CreateChildLogger(string name)
		{
			return new DebugLogger(this.loggerName + "." + name);
		}

        protected override void LogFormat(LogLevel level, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(
                Thread.CurrentThread.Name + " " +
                "[" + level + "] " + loggerName + " - " + format, args);
        }

        protected override bool IsLevelEnabled(AbstractLogger.LogLevel logLevel)
        {
            return true;
        }
    }
}
