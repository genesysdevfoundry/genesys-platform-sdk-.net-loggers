﻿/*

 Disclaimer: IMPORTANT:  This sample software is supplied to you by Genesys
 Telecommunications Laboratories Inc ("Genesys") in consideration of your agreement
 to the following terms, and your use, installation, modification or redistribution
 of this Genesys software constitutes acceptance of these terms.  If you do not
 agree with these terms, please do not use, install, modify or redistribute this
 Genesys software.
 
 In consideration of your agreement to abide by the following terms, and subject
 to these terms, Genesys grants you a personal, non-exclusive license, under
 Genesys's copyrights in this original Genesys software (the "Genesys Software"), to
 use, reproduce, modify and redistribute the Genesys Software, with or without
 modifications, in source and/or binary forms; provided that if you redistribute
 the Genesys Software in its entirety and without modifications, you must retain
 this notice and the following text and disclaimers in all such redistributions
 of the Genesys Software.
 
 Neither the name, trademarks, service marks or logos of Genesys Inc. may be used
 to endorse or promote products derived from the Genesys Software without specific
 prior written permission from Genesys.  Except as expressly stated in this notice,
 no other rights or licenses, express or implied, are granted by Genesys herein,
 including but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Genesys Software may be
 incorporated.
 
 The Genesys Software is provided by Genesys on an "AS IS" basis.  GENESYS MAKES NO
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDING THE GENESYS SOFTWARE OR ITS USE AND OPERATION ALONE OR IN
 COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL GENESYS BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR
 DISTRIBUTION OF THE GENESYS SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF
 CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF
 GENESYS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Genesys Inc. All Rights Reserved.
 
*/

using System;
using Genesyslab.Platform.Commons.Logging;
using System.Diagnostics;

namespace Sample.Platform.Logging
{
	/// <summary>
	/// <para>
	/// Implements Platform SDK logging by using .NET TraceSources.
	/// </para>
	/// <para>
	/// Platform SDK loggers build a hierarchy. There is no such concept of hierarchy in .NET TraceSources.
	/// For example, a PSDK logger named "TServer" is the parent of "TServer.HAConnection", but that is not
	/// the case for TraceSources.
	/// </para>
	/// <para>
	/// Therefore, this class emulates such a hierarchy, which is best described with an example. Let's imagine
	/// TraceSources configured this way:
	/// <code>
	///   <system.diagnostics>
	///    <sources>
	///      <source name="GenesysTServer" switchValue="Verbose" />
	///      <source name="GenesysTServer.HAConnection" switchValue="Information" />
	///    </sources>
	///  </system.diagnostics>
	/// </code>
	/// Protocol logging configured programmatically this way:
	/// <code>
	/// tserverProtocol.EnableLogging(new TraceSourceLogger(new TraceSource("GenesysTServer")));
	/// </code>
	/// </para>
	/// <para>
	/// PSDK loggers will work this way, in order of priority:
	/// <list type="number">
	/// <item>
	/// <term>HAConnection.*</term>
	/// <description>Will log using TraceSource "GenesysTServer.HAConnection"</description>
	/// </item>
	/// <item>
	/// <term>*</term>
	/// <description>Will log using TraceSource "GenesysTServer"</description>
	/// </item>
	/// </list>
	/// </para>
	/// </summary>
	/// <remarks>
	/// TraceSources can only be detected as configured if they have a non-default configuration.
	/// So for example, this configuration of a PSDK child logger is not taken into account:
	/// <code>
	/// <source name="GenesysTServer.HAConnection" switchValue="Off" />
	/// </code>
	/// You need to configure a different switch level, or add listeners or attributes to the TraceSource
	/// in order for it to be considered.
	/// </remarks>
	public class TraceSourceLogger : AbstractLogger
	{
		protected readonly TraceSource traceSource;
		protected readonly string rootTraceSourceName;
		protected readonly string loggerName;

		protected TraceSourceLogger(TraceSource traceSource, string rootTraceSourceName, string loggerName)
		{
			this.traceSource = traceSource;
			this.rootTraceSourceName = rootTraceSourceName;
			this.loggerName = loggerName;
		}

		public TraceSourceLogger(TraceSource traceSource) :
			this(traceSource, traceSource.Name, "") { }

		public override ILogger CreateChildLogger(string name)
		{
			string childLoggerName = loggerName.Length == 0 ? name : loggerName + "." + name;
			var childTraceSource = new TraceSource(rootTraceSourceName + "." + childLoggerName);

			if (IsTraceSourceNotConfigured(childTraceSource))
				return new TraceSourceLogger(traceSource, rootTraceSourceName, childLoggerName);
			else
				return new TraceSourceLogger(childTraceSource, rootTraceSourceName, childLoggerName);
		}

		static bool IsTraceSourceNotConfigured(TraceSource traceSource)
		{
			// check for TraceSource default values
			return
				traceSource.Switch.Level == SourceLevels.Off &&
				traceSource.Switch.DisplayName == traceSource.Name &&
				traceSource.Switch.Description.Length == 0 &&
				traceSource.Listeners.Count == 1 &&
				traceSource.Listeners[0] is DefaultTraceListener &&
				traceSource.Listeners[0].Name == "Default" &&
				traceSource.Attributes.Count == 0;
		}
		
		static TraceEventType ConvertToTraceEventType(LogLevel level)
		{
			switch (level)
			{
				case LogLevel.Debug: return TraceEventType.Verbose;
				case LogLevel.Info: return TraceEventType.Information;
				case LogLevel.Warn: return TraceEventType.Warning;
				case LogLevel.Error: return TraceEventType.Error;
				case LogLevel.FatalError: return TraceEventType.Critical;
				default: throw new Exception("LogLevel value not supported: " + level);
			}
		}

		protected override void LogMessage(LogLevel level, object message)
		{
			int id = 0;
			string prefix = loggerName.Length == 0 ? "" : loggerName + ": ";
			traceSource.TraceEvent(ConvertToTraceEventType(level), id, LoggerPrefix + message);
		}

		protected override void LogFormat(LogLevel level, string format, params object[] args)
		{
			int id = 0;
			// For this to work, it is assumed that logger names do not include formatting escape characters '{' and '}'.
			traceSource.TraceEvent(ConvertToTraceEventType(level), id, LoggerPrefix + format, args);
		}

		protected override void LogException(LogLevel level, object message, Exception exception)
		{
			int id = 0;
			traceSource.TraceData(ConvertToTraceEventType(level), id, LoggerPrefix + message, exception);
		}

		string LoggerPrefix
		{
			get { return loggerName.Length == 0 ? "" : loggerName + ": "; }
		}

		protected override bool IsLevelEnabled(LogLevel logLevel)
		{
			var traceEventType = ConvertToTraceEventType(logLevel);
			var sourceLevel = traceSource.Switch.Level;
			return ((int)traceEventType & (int)sourceLevel) != 0;
		}
	}
}
